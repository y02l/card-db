import json
import requests


def download_card_data():
    url = 'https://file.slimemoss.com/yugioh-card/card-data.json'
    resp = requests.get(url)
    return json.loads(resp.text)


"""
{
  name: {
    is02: str
    ruby: str
  }
}
"""
card_data = download_card_data()

__all__ = [card_data]
