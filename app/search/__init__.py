from . import main


def search(q='', only02=False):
    '''
    いい感じにカード名検索する

    Parameters
    ----------
    q
      string : クエリ
    only02
      bool : 02のみ検索

    Returns
    -------
    [name]
      name : string : 公式DB準拠のカード名
      求められているであろうカード名順にソート
    '''
    return main.get(q, only02)


__all__ = [search]
