import re

import jaconv

import static_data


# A-Z, 大文字ひらがな, 0-9
def simple_str(v):
    v = v.upper()
    v = jaconv.kata2hira(v)
    v = re.sub(r'[^A-Z0-9あ-んー]*', r'', v)
    return v


# [dict(name, is02, ruby, fullruby)]
data = [
    dict(name=name, **data, fullruby=simple_str(data['ruby']))
    for name, data in static_data.card_data.items()
]


def my_own_algo(data, query):
    score = 0
    for query_block_start in range(len(query) + 1):
        for query_block_end in range(query_block_start, len(query) + 1):
            query_block = query[query_block_start:query_block_end]
            if query_block in data:
                score += len(query_block)
    return score


def quick_my_own_algo(data, query):
    score = 0
    score += (100 if query in data else 0)
    for q in query:
        score += (1 if q in data else 0)
    return score


def get_one_func(query, keys, func, cards):
    for c in cards:
        c['score_list'] = []

    for key in keys:
        _query = simple_str(query) if key == 'fullruby' else query
        for c in cards:
            score = func(c[key], _query)
            c['score_list'].append(score)

    cards = sorted(
        cards, key=lambda c:
        (-1 * max(c['score_list']), c['name']))[:max((int)(len(cards) /
                                                           100), 20)]
    return cards


def get(query,
        only02=False,
        keys=['name', 'fullruby'],
        funcs=[quick_my_own_algo, my_own_algo]):
    cards = [d for d in data.copy() if not only02 or d['is02']]
    for func in funcs:
        cards = get_one_func(query, keys, func, cards)

    cards = [c['name'] for c in cards]

    return cards
