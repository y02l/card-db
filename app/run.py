from distutils import util
from flask import Flask, jsonify, request
from werkzeug.middleware.profiler import ProfilerMiddleware

import static_data
from search import search

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False

if app.debug:
    app.config['PROFILE'] = True
    app.wsgi_app = ProfilerMiddleware(app.wsgi_app,
                                      sort_by=['tottime'],
                                      restrictions=[20])


def requestBodyList(request):
    '''
    リストを返すリクエストの共通リクエストボディー
    '''
    size = request.args.get('size', default=100, type=int)
    start = request.args.get('start', default=0, type=int)
    return size, start


def responseList(data, data_key, size, start):
    '''
    リストを返すリクエストの共通レスポンス
    '''
    res = dict()
    res[data_key] = data[start:start + size]
    if len(data) > start + size:
        res['next'] = start + size
    return res


@app.route('/search', methods=['GET'])
def get_search():
    size, start = requestBodyList(request)

    q = request.args.get('q', default='', type=str)
    only02 = util.strtobool(request.args.get('only02', default='false', type=str))
    data = search(q, only02)

    res = responseList(data, 'data', size, start)
    return jsonify(res), 200


@app.route('/card/<string:name>', methods=['GET'])
def get_card(name):
    if name not in static_data.card_data:
        return '', 404

    res = static_data.card_data[name]
    res['name'] = name

    return jsonify(res), 200
