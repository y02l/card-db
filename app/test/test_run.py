import json
import time

import pytest

from run import app


@pytest.fixture(scope='function', autouse=True)
def prepost():
    app.config['TESTING'] = True
    yield
    pass


def test_get_card_valid():
    client = app.test_client()
    url = '/card/ブラック・マジシャン'
    resp = client.get(url, content_type='application/json')
    data = json.loads(resp.get_data().decode('UTF-8'))
    assert resp.status_code == 200
    assert data['name'] == 'ブラック・マジシャン'
    assert data['is02']


def test_get_card_invalid():
    client = app.test_client()
    url = '/card/ブラック・マジシャン2'
    resp = client.get(url, content_type='application/json')
    assert resp.status_code == 404


@pytest.mark.parametrize("q, ans", [
    ('ゴブ突', 'ゴブリン突撃部隊'),
    ('ですがーでぃうす', '仮面魔獣デス・ガーディウス'),
    ('リヴェンデットスレイヤー', 'リヴェンデット・スレイヤー')
])
def test_search(q, ans):
    client = app.test_client()
    resp = client.get('/search?q=' + q)
    data = json.loads(resp.get_data().decode('UTF-8'))
    assert data['data'][0] == ans


@pytest.mark.parametrize("q, ans", [
    ('リヴェンデットスレイヤー', 'リヴェンデット・スレイヤー')
])
def test_search_only02(q, ans):
    client = app.test_client()
    resp = client.get('/search?q=' + q + '&only02=false')
    data = json.loads(resp.get_data().decode('UTF-8'))
    assert data['data'][0] == ans
    resp = client.get('/search?q=' + q + '&only02=true')
    data = json.loads(resp.get_data().decode('UTF-8'))
    assert data['data'][0] != ans


def test_search_time():
    client = app.test_client()
    time_start = time.time()
    client.get('/search?q=' + 'ゴブ突')
    elapse = time.time() - time_start
    print('search time : ' + str(elapse) + ' sec')
