FROM python:3.8-alpine

COPY ./app/requirements.txt /app/
RUN pip install -r /app/requirements.txt

COPY ./app /app

# start flask
ENV FLASK_APP='/app/run.py'
ENTRYPOINT ["flask", "run", "--host", "0.0.0.0", "--port", "80"]
