# 遊戯王カードに関するサービス

## クイックスタート
```
git clone git@gitlab.com:y02l/card-db.git; \
cd card-db; \
docker-compose up -d; \
curl -G http://$(docker port card-db_card-db_1 80)/search --data-urlencode "q=ぶらまじ"
```

## ローカルでのテスト
```
docker-compose exec card-db pytest /app/test -s
```

## APIリファレンス

### カード情報取得
### いい感じのカード名検索
#### HTTPリクエスト
GET http://card-db/card/{name}
#### パスパラメータ
* name : str : カード名
#### レスポンス
```
{
  name: 黒魔術のカーテン,
  ruby: くろまじゅつのカーテン,
  is02: true
}
```


### いい感じのカード名検索
#### HTTPリクエスト
GET http://card-db/search?q={q}
#### クエリパラメータ
* q : str : クエリ
* only02 : bool : 02カードのみ検索
* size : int : 返却するリストのサイズ : default 100
* start : str : 継続トークンを設定します。
#### レスポンス
* next : str : 継続トークンです。このレスポンスでカード名が取得しきれなかったときに返されます。
```
{
  next: fj3n15dwejk7n1
  data: [
    ブラック・マジシャン,
	ブラック・マジシャン・ガール,
	ブラッド・マジシャン－煉獄の魔術師－,
    ...
  ]
}
```
